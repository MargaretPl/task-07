package com.epam.rd.java.basic.task7.db;

import java.util.*;
import java.util.logging.*;
import java.io.*;
import java.sql.*;


import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final Logger LOGS = Logger.getLogger(DBManager.class.getName());
	private static final String URL;
	private static DBManager check;

	public static synchronized DBManager getInstance() {
		if (check == null) {
			check = new DBManager();
		}
		return check;
	}

	private DBManager() {
	}



	static {
		Properties prop = new Properties();
		String temp = "";
		try (InputStream input = new FileInputStream("app.properties")) {
			prop.load(input);
			temp = prop.getProperty("connection.url");
			LOGS.fine("app.properties loaded");
		} catch (IOException e) {
			LOGS.log(Level.SEVERE, "Failed loading app.properties: ", e);
		}
		URL = temp;
	}

	//-----------------------------------------------------------------------------------
	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			for (Team t : teams) {
				setTeamForUserCheck(con, user, t);
			}
			con.commit();
		} catch (SQLException ex) {
			if (con != null) {
				LOGS.log(Level.SEVERE, "Cannot add user in two teams in method setTeamsForUser(): ", ex);
				try {
					con.rollback();
				} catch (SQLException exs) {
					throw new DBException("DB setTeamsForUser() problem", exs);
				}
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					throw new DBException("DB setTeamsForUser() problem", ex);
				}
			}
		}
		return true;
	}

	//-----------------------------------------------------------------------------------
	private static boolean setTeamForUserCheck(Connection con, User user, Team team) throws SQLException {
		try {
			setTeamForUser(con, user, team);
		} catch (DBException ex) {
			LOGS.log(Level.SEVERE, "Failed commit in method setTeamForUserCheck: ", ex);
			con.rollback();
			con.close();
			return false;
		}
		return true;
	}

	//-----------------------------------------------------------------------------------
	private static void setTeamForUser(Connection con, User user, Team team) throws DBException {
		try (PreparedStatement statement =
					 con.prepareStatement("INSERT INTO users_teams VALUES (?, ?)")) {
			int k = 1;
			statement.setInt(k++, user.getId());
			statement.setInt(k, team.getId());
			statement.executeUpdate();

		} catch (SQLException e) {
			LOGS.log(Level.SEVERE, "Failed in method setTeamForUser(): ", e);
			throw new DBException("DB setTeamForUser() problem", e);
		}
	}

	//-----------------------------------------------------------------------------------
	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		ResultSet rs = null;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement statement = con.prepareStatement("SELECT * FROM teams WHERE teams.id " +
					 "= ANY (SELECT team_id FROM users_teams WHERE user_id=?)")) {

			statement.setInt(1, user.getId());
			rs = statement.executeQuery();
			while (rs.next()) {
				teams.add(extractTeam(rs));
			}
		} catch (SQLException ex) {
			LOGS.log(Level.SEVERE, "Failed in method getUserTeams(): ", ex);
			throw new DBException("DB getUserTeams() problem", ex);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					LOGS.log(Level.SEVERE, "Failed in method getUserTeams() -> rs.close(): ", e);
				}
			}
		}
		return teams;
	}

	//-----------------------------------------------------------------------------------
	private static User extractUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setId(rs.getInt("id"));
		user.setLogin(rs.getString("login"));
		return user;
	}

	//-----------------------------------------------------------------------------------
	private static Team extractTeam(ResultSet rs) throws SQLException {
		Team team = new Team();
		team.setId(rs.getInt("id"));
		team.setName(rs.getString("name"));
		return team;
	}

	//-----------------------------------------------------------------------------------
	public boolean deleteTeam(Team team) throws DBException {
		boolean result;

		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement statement =
					 con.prepareStatement("DELETE FROM teams WHERE name=?")) {
			int k = 1;
			statement.setString(k, team.getName());
			result = statement.executeUpdate() > 0;
		} catch (SQLException ex) {
			throw new DBException("DB deleteTeam() problem", ex);
		}
		return result;

	}

	//-----------------------------------------------------------------------------------
	public boolean updateTeam(Team team) throws DBException {
		boolean result;

		try {
			Connection con = DriverManager.getConnection(URL);
			PreparedStatement statement =
					con.prepareStatement("UPDATE teams SET name=? WHERE id=?");

			int k = 1;
			statement.setString(k++, team.getName());
			statement.setInt(k, team.getId());

			result = statement.executeUpdate() > 0;
		} catch (SQLException ex) {
			throw new DBException("DB updateTeam() problem", ex);
		}
		return result;
	}
	//-----------------------------------------------------------------------------------
	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try {
			Connection connection = DriverManager.getConnection(URL);
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM users rs");
			while (rs.next()) {
				users.add(extractUser(rs));
			}
			users.sort(Comparator.comparingInt(User::getId));
		} catch (SQLException e) {
			throw new DBException("findAllUsers() failed", e);
		}
		return users;
	}

	//-----------------------------------------------------------------------------------
	public boolean insertUser(User user) throws DBException {
		boolean res = false;
		ResultSet rs = null;
		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement =
					 connection.prepareStatement("INSERT INTO users VALUES (DEFAULT, ?)",
							 Statement.RETURN_GENERATED_KEYS)) {
			int k = 1;
			statement.setString(k, user.getLogin());

			if (statement.executeUpdate() > 0) {
				rs = statement.getGeneratedKeys();
				if (rs.next()) {
					int id = rs.getInt(1);
					user.setId(id);
					res = true;
				}
			}
		} catch (SQLException e) {
			LOGS.log(Level.SEVERE, "DB insertUser() problem", e);
			throw new DBException("DB insertUser() problem", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					LOGS.log(Level.SEVERE, "DB insertUser() -> rs.close() problem", e);
				}
			}
		}
		return res;
	}

	//-----------------------------------------------------------------------------------
	public boolean deleteUsers(User... users) throws DBException {
		boolean result = true;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement statement =
					 con.prepareStatement("DELETE FROM users WHERE login=?")) {
			int k = 1;
			for (User u : users) {
				statement.setString(k, u.getLogin());
				result = result && statement.executeUpdate() > 0;
			}
		} catch (SQLException ex) {
			throw new DBException("DB deleteUsers() problem", ex);
		}
		return result;
	}

	//-----------------------------------------------------------------------------------
	public User getUser(String login) throws DBException {
		User user = null;
		ResultSet rs = null;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement statement =
					 con.prepareStatement("SELECT id, login FROM users WHERE login=?")) {

			statement.setString(1, login);
			rs = statement.executeQuery();
			if (rs.next()) {
				user = extractUser(rs);
			}
		} catch (SQLException e) {
			LOGS.log(Level.SEVERE, "getUser() SQLException ", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException ex) {
					throw new DBException("DB getUser() problem", ex);
				}
			}
		}
		return user;
	}

	//-----------------------------------------------------------------------------------
	public Team getTeam(String name) throws DBException {
		Team team = null;
		ResultSet rs = null;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement statement =
					 con.prepareStatement("SELECT id, name FROM teams WHERE name=?")) {

			statement.setString(1, name);
			rs = statement.executeQuery();
			if (rs.next()) {
				team = extractTeam(rs);
			}
		} catch (SQLException e) {
			LOGS.log(Level.SEVERE, "getTeam() SQLException ", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException ex) {
					throw new DBException("DB getTeam() problem", ex);
				}
			}
		}
		return team;
	}

	//-----------------------------------------------------------------------------------
	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(URL);
			 Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery("SELECT * FROM teams")) {
			while (rs.next()) {
				teams.add(extractTeam(rs));
			}
		} catch (SQLException ex) {
			throw new DBException("DB findAllTeams() problem", ex);
		}
		return teams;
	}

	//-----------------------------------------------------------------------------------
	public boolean insertTeam(Team team) throws DBException {
		boolean res = false;
		ResultSet rs = null;
		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement =
					 connection.prepareStatement("INSERT INTO teams VALUES (DEFAULT, ?)",
							 Statement.RETURN_GENERATED_KEYS)) {
			int k = 1;
			statement.setString(k, team.getName());
			if (statement.executeUpdate() > 0) {
				rs = statement.getGeneratedKeys();
				if (rs.next()) {
					int id = rs.getInt(1);
					team.setId(id);
					res = true;
				}
			}
		} catch (SQLException e) {
			LOGS.log(Level.SEVERE, "DB insertTeam() problem ", e);
			throw new DBException("DB insertTeam() problem", e);

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					LOGS.log(Level.SEVERE, "DB insertTeam() -> rs.close() problem ", e);
				}
			}
		}
		return res;
	}

}
